/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/24 18:42:08 by atheveno          #+#    #+#             */
/*   Updated: 2016/01/09 14:30:58 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/libft.h"

char	*ft_strncat(char *dest, const char *src, size_t n)
{
	size_t	index1;
	size_t	index2;
	size_t	lim;

	lim = 0;
	index1 = 0;
	index2 = 0;
	while (dest[index1])
		index1++;
	while (src[index2] && lim < n)
	{
		dest[index1 + index2] = src[index2];
		index2++;
		lim++;
	}
	dest[index1 + index2] = 0;
	return (dest);
}
