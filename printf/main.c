/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/16 18:39:37 by atheveno          #+#    #+#             */
/*   Updated: 2016/02/16 23:23:05 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "ft_printf.h"
#include "handle_funcs.h"
#include "parse_handlers.h"
#include "utils.h"

int	main(void)
{
	printf("{%#.5x}\n", 1);
	ft_printf("{%#.5x}\n", 1);
	return (0);
}
