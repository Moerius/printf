/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 16:20:22 by atheveno          #+#    #+#             */
/*   Updated: 2016/01/09 14:33:44 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/libft.h"

int		ft_isspace42(char c)
{
	return ((c == ' ' || c == '\t' || c == '\n') ? 1 : 0);
}

char	*ft_strtrim(char const *s)
{
	char	*tmp;
	size_t	index;

	index = 0;
	tmp = (char *)malloc(sizeof(*s) * ft_strlen(s) + 1);
	if (tmp == NULL)
		return (NULL);
	ft_strcpy(tmp, s);
	while (ft_isspace42(tmp[index]))
		index++;
	tmp = ft_strsub(tmp, index, ft_strlen(tmp) - index);
	tmp = ft_strrev(tmp);
	index = 0;
	while (ft_isspace42(tmp[index]))
		index++;
	tmp = ft_strsub(tmp, index, ft_strlen(tmp) - index);
	tmp = ft_strrev(tmp);
	return (tmp);
}
