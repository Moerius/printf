/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_handlers.h                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/16 18:14:52 by atheveno          #+#    #+#             */
/*   Updated: 2016/02/16 18:14:53 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PARSE_HANDLERS_H
# define PARSE_HANDLERS_H
# include "handle_funcs.h"

char	*parse_flags(char **format, t_arg *arg);
char	*parse_width(char **format, va_list *list, t_arg *arg);
char	*parse_precision(char **format, va_list *list, t_arg *arg);
char	*parse_length(char **format, t_arg *arg);
#endif
