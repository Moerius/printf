/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_gen.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/12 10:03:42 by atheveno          #+#    #+#             */
/*   Updated: 2016/02/12 10:04:00 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

size_t	ft_print_gen(t_pfvar *var, const char *pre, const char *base)
{
	size_t			bytes;
	unsigned long	n;
	ssize_t			len;
	int				l;

	bytes = 0;
	n = get_arg_long(var);
	if (!n)
		var->flag |= ~FT_PRINTF_FLAG_DIESE;
	l = ft_log(n, ft_strlen(base));
	len = ft_max(var->precision, l);
	bytes += ft_fillspc(1, var, len);
	bytes += ft_fillzero(var, l);
	if (var->flag & FT_PRINTF_FLAG_DIESE)
		bytes += ft_putstr_fd(var->fd, pre);
	bytes += ft_putul(var->fd, n, base);
	bytes += ft_fillspc(0, var, len);
	return (bytes);
}
