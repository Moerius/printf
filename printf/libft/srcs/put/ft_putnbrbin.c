/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbrbin.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/10 18:39:32 by atheveno          #+#    #+#             */
/*   Updated: 2016/01/27 14:27:23 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/libft.h"

static const char	*g_binbase = "01";

int		ft_putnbrbin(unsigned long long nb)
{
	char	nb_act;
	int		size;

	nb_act = nb % 2;
	size = 1;
	if (nb >= 2)
		size += ft_putnbrbin(nb / 2);
	ft_putchar(g_binbase[(int)nb_act]);
	return (size);
}
