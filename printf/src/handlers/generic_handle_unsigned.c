/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   generic_handle_unsigned.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/16 18:16:01 by atheveno          #+#    #+#             */
/*   Updated: 2016/02/16 23:09:01 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdint.h>
#include <sys/types.h>
#include <libft.h>
#include "ft_printf.h"
#include "handle_funcs.h"
#include "utils.h"

static void	ft_putnbrbp(uintmax_t nbr, char *base, t_arg *arg, unsigned nbr_len)
{
	if (arg->got_precision)
		width_pad(nbr_len, arg->precision, base[0], arg);
	if (nbr == 0 && arg->got_precision && arg->precision == 0)
		return ;
	else
		ft_putnbr_base(nbr, base);
}

ssize_t		handle_uint(uintmax_t nbr, t_arg *arg, char *base, char *prefix)
{
	unsigned int			nbr_len;
	unsigned int			nbrstrlen;

	if (arg->got_precision)
		arg->pad_zeroes = 0;
	nbr_len = nbrlen(nbr, base);
	if (arg->got_width && !arg->right_pad && arg->pad_zeroes)
	{
		if (arg->got_precision)
			arg->precision = ft_max(arg->width, arg->precision);
		else
			arg->precision = ft_max(arg->width, nbr_len);
		arg->got_precision = 1;
		arg->got_width = 0;
	}
	nbrstrlen = calc_nbrstrlen(nbr, base, prefix, arg);
	if (arg->got_width && !arg->right_pad)
		width_pad(nbrstrlen, arg->width, ' ', arg);
	if (arg->force_prefix && prefix != NULL && nbr != 0)
		if ((nbr_len += ft_putstr(prefix)) && arg->width && !arg->right_pad)
			nbrstrlen -= ft_strlen(prefix);
	ft_putnbrbp(nbr, base, arg, nbr_len);
	if (arg->got_width && arg->right_pad)
		width_pad(nbrstrlen, arg->width, ' ', arg);
	return (arg->got_width ? ft_max(nbrstrlen, arg->width) : nbrstrlen);
}
