/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putstr_all.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/12 10:13:18 by atheveno          #+#    #+#             */
/*   Updated: 2016/02/12 10:17:30 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

size_t			ft_print_str(t_pfvar *var)
{
	size_t	bytes;
	ssize_t	len;
	char	*p;
	int		i;

	i = 0;
	bytes = 0;
	p = va_arg(var->ap, char *);
	if (!p)
		p = "(null)";
	len = (var->precision >= 0 ? var->precision : (ssize_t)ft_strlen(p));
	bytes += ft_fillspc(1, var, len);
	while (i < len && *p)
	{
		bytes += ft_putchar_fd(var->fd, *p);
		p++;
		i++;
	}
	bytes += ft_fillspc(0, var, len);
	return (bytes);
}

static int		ft_puthex(int fd, int c)
{
	if (c < 10)
		ft_putchar_fd(fd, '0' + c);
	else
		ft_putchar_fd(fd, 'a' + c - 10);
	return (1);
}

static size_t	ft_spestrlen(const char *str)
{
	char	*p;
	size_t	bytes;

	bytes = 0;
	p = (char *)str;
	while (*p)
	{
		if (*p >= ' ' && *p <= '~')
			bytes += 3;
		else
			bytes++;
		p++;
	}
	return (bytes);
}

static size_t	ft_putspechar(const int fd, const char c)
{
	size_t	bytes;
	int		n;

	bytes = 0;
	if (c >= ' ' && c <= '~')
		bytes += ft_putchar_fd(fd, c);
	else
	{
		n = ((int)c & UCHAR_MAX);
		bytes += ft_putchar_fd(fd, '\\');
		bytes += ft_puthex(fd, n / 64);
		n %= 64;
		bytes += ft_puthex(fd, n / 8);
		bytes += ft_puthex(fd, n % 8);
	}
	return (bytes);
}

size_t			ft_print_spestr(t_pfvar *var)
{
	char	*p;
	size_t	bytes;
	ssize_t	len;
	int		i;

	i = 0;
	bytes = 0;
	p = va_arg(var->ap, char *);
	if (!p)
		p = "(null)";
	len = (var->precision >= 0 ? var->precision : (ssize_t)ft_spestrlen(p));
	bytes += ft_fillspc(1, var, len);
	while (i < len && *p)
	{
		bytes += ft_putspechar(var->fd, *p);
		p++;
		i++;
	}
	bytes += ft_fillspc(0, var, len);
	return (bytes);
}
