/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_vfdprintf.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/12 10:15:46 by atheveno          #+#    #+#             */
/*   Updated: 2016/02/12 10:31:01 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

static const char	*get_flags(const char *p, int *flags)
{
	char	*q;

	*flags = 0;
	while ((q = ft_strchr(FT_PRINTF_FLAGS, *p)) != NULL)
	{
		*flags |= (1 << (q - FT_PRINTF_FLAGS));
		p++;
	}
	if (*flags & FT_PRINTF_FLAG_MINUS)
		*flags &= ~FT_PRINTF_FLAG_ZERO;
	if (*flags & FT_PRINTF_FLAG_PLUS)
		*flags &= ~FT_PRINTF_FLAG_SPACE;
	return (p);
}

static const char	*get_token(const char *p, t_pfvar *var)
{
	p = get_flags(p, &var->flag);
	var->width = ft_pos(ft_strtol(p, &p, 10));
	if (*p == '.')
	{
		p++;
		var->precision = ft_pos(ft_strtol(p, &p, 10));
	}
	else
		var->precision = -1;
	ft_memset(var->length, 0, 3);
	if (*p == 'h' || *p == 'l')
		var->length[0] = *p++;
	if (*p == 'h' || *p == 'l')
		var->length[1] = *p++;
	var->type = *p++;
	return (p);
}

static const char	*handle_token(const char *p, size_t *bytes,
		t_pfvar *var)
{
	var->bytes = *bytes;
	if (*p == '%')
	{
		p++;
		p = get_token(p, var);
		(*bytes) += ft_print_token(var);
	}
	else
	{
		ft_putchar_fd(var->fd, *p);
		(*bytes)++;
		p++;
	}
	return (p);
}

int					ft_vfdprintf(const int fd, const char *format, va_list ap)
{
	const char	*p;
	size_t		bytes;
	t_pfvar		var;

	bytes = 0;
	ft_memset(&var, 0, sizeof(var));
	va_copy(var.ap, ap);
	var.fd = fd;
	p = format;
	while (*p)
		p = handle_token(p, &bytes, &var);
	return (bytes);
}
