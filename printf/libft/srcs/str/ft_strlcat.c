/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/10/20 05:45:07 by atheveno          #+#    #+#             */
/*   Updated: 2016/01/09 14:29:59 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/libft.h"

size_t		ft_strlcat(char *dest, const char *src, size_t size)
{
	char		*tmpd;
	const char	*tmps;
	size_t		count;
	size_t		maxnlen;

	tmpd = dest;
	tmps = src;
	count = size;
	while (count-- && *tmpd)
		tmpd++;
	maxnlen = tmpd - dest;
	count = size - maxnlen;
	if (!count)
		return (maxnlen + ft_strlen(tmps));
	while (*tmps)
	{
		if (count != 1)
		{
			*tmpd++ = *tmps;
			count--;
		}
		tmps++;
	}
	*tmpd = 0;
	return (maxnlen + (tmps - src));
}
