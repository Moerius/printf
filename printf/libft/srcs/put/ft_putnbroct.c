/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbroct.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/10 18:05:16 by atheveno          #+#    #+#             */
/*   Updated: 2016/01/27 14:21:15 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/libft.h"

static const char	*g_octbase = "01234567";

int		ft_putnbroct(unsigned long long nb, unsigned int len)
{
	char	nb_act;
	int		size;

	nb_act = nb % 8;
	size = 1;
	if (nb >= 8)
		size += ft_putnbroct(nb / 8, len > 0 ? len - 1 : 0);
	else if (len > 0)
	{
		size += len - 1;
		while (--len > 0)
			ft_putchar('0');
	}
	ft_putchar(g_octbase[(int)nb_act]);
	return (size);
}
