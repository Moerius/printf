/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_log.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/12 10:00:36 by atheveno          #+#    #+#             */
/*   Updated: 2016/02/12 10:00:52 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

int	ft_log(unsigned long n, int base)
{
	unsigned int	size;

	if (!n)
		return (1);
	size = 0;
	while (n)
	{
		n /= base;
		size++;
	}
	return (size);
}
