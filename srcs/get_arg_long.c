/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_arg_long.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/12 10:10:22 by atheveno          #+#    #+#             */
/*   Updated: 2016/02/12 10:10:33 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

unsigned long	get_arg_long(t_pfvar *var)
{
	unsigned long n;

	if (*var->length == 'l')
		n = va_arg(var->ap, long);
	else
		n = va_arg(var->ap, int);
	if (var->length[0] == 'h')
		n = n & USHRT_MAX;
	if (var->length[1] == 'h')
		n = n & UCHAR_MAX;
	return (n);
}
