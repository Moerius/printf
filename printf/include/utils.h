/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/16 18:14:58 by atheveno          #+#    #+#             */
/*   Updated: 2016/02/16 21:00:54 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef UTILS_H
# define UTILS_H
# include <inttypes.h>
# include "handle_funcs.h"

uintmax_t		get_unsigned_from_length(va_list *args, t_arg *arg);
void			width_pad(int nbrstrlen, int width, char padwith, t_arg *arg);
unsigned int	nbrlen(uintmax_t nbr, char *base);
unsigned int	calc_nbrstrlen(uintmax_t nbr, char *base, char *p, t_arg *arg);
ssize_t			nbrforceprefix(uintmax_t n, char *b, t_arg *a, char *p);
#endif
