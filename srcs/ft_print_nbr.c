/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_nbr.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/12 10:04:06 by atheveno          #+#    #+#             */
/*   Updated: 2016/02/12 10:23:17 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

size_t	ft_printbin(t_pfvar *var)
{
	return (ft_print_gen(var, g_digits[BIN_PREV], g_digits[BIN_BASE]));
}

size_t	ft_printoct(t_pfvar *var)
{
	return (ft_print_gen(var, g_digits[OCT_PREV], g_digits[OCT_BASE]));
}

size_t	ft_printdec(t_pfvar *var)
{
	return (ft_print_gen(var, g_digits[DEC_PREV], g_digits[DEC_BASE]));
}

size_t	ft_printhex(t_pfvar *var)
{
	return (ft_print_gen(var, g_digits[HEX_PREV], g_digits[HEXB_BASE]));
}

size_t	ft_printhexb(t_pfvar *var)
{
	return (ft_print_gen(var, g_digits[HEXB_PREV], g_digits[HEXB_BASE]));
}
