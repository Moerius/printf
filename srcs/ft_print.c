/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/12 10:02:14 by atheveno          #+#    #+#             */
/*   Updated: 2016/02/12 10:03:25 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

static ssize_t	calc_len(long n, int logn, t_pfvar *var)
{
	ssize_t	len;

	len = ft_max(var->precision, logn);
	if (n < 0 || ((var->flag & FT_PRINTF_FLAG_PLUS
					|| var->flag & FT_PRINTF_FLAG_SPACE) && n > 0))
		len++;
	if (var->flag & FT_PRINTF_FLAG_ZERO)
	{
		var->precision = ft_pos(var->precision) + ft_pos(var->width - len);
		var->precision += logn;
		len = var->width;
	}
	return (len);
}

size_t			ft_print_char(t_pfvar *var)
{
	size_t	bytes;
	long	n;
	ssize_t	len;

	var->length[0] = 'h';
	var->length[1] = 'h';
	n = get_arg_long(var);
	len = calc_len(n, 1, var);
	ft_fillspc(1, var, len);
	if (var->flag & FT_PRINTF_FLAG_PLUS && n > 0)
		ft_putchar_fd(var->fd, '+');
	if (var->flag & FT_PRINTF_FLAG_SPACE && n > 0)
		ft_putchar_fd(var->fd, ' ');
	ft_fillzero(var, 1);
	bytes = ft_putchar_fd(var->fd, n);
	ft_fillspc(0, var, len);
	return (bytes);
}

size_t			ft_print_sdec(t_pfvar *var)
{
	size_t	bytes;
	long	n;
	ssize_t	len;
	int		logn;

	n = get_arg_long(var);
	logn = ft_log(n, 10);
	len = calc_len(n, logn, var);
	ft_fillspc(1, var, len);
	if (var->flag & FT_PRINTF_FLAG_PLUS && n > 0)
		ft_putchar_fd(var->fd, '+');
	if (var->flag & FT_PRINTF_FLAG_SPACE && n > 0)
		ft_putchar_fd(var->fd, ' ');
	ft_fillzero(var, logn);
	bytes = ft_putl(var->fd, n, "0123456789");
	ft_fillspc(0, var, len);
	return (bytes);
}

size_t			ft_print_n(t_pfvar *var)
{
	int	*n;

	n = va_arg(var->ap, int *);
	*n = var->bytes;
	return (0);
}

size_t			ft_print_p(t_pfvar *var)
{
	var->flag |= FT_PRINTF_FLAG_DIESE;
	return (ft_print_gen(var, "0x", "0123456789abcdef"));
}
