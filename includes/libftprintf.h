/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libftprintf.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/12 10:18:23 by atheveno          #+#    #+#             */
/*   Updated: 2016/02/12 10:22:41 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFTPRINTF_H
# define LIBFTPRINTF_H

# include <unistd.h>
# include <stdlib.h>
# include <stdarg.h>
# include <string.h>
# include <limits.h>

# define FT_PRINTF_FLAG_DELIM	'%'

# define FT_PRINTF_FLAGS		"#-+ *0"
# define FT_PRINTF_FLAG_DIESE	1
# define FT_PRINTF_FLAG_MINUS	2
# define FT_PRINTF_FLAG_PLUS	4
# define FT_PRINTF_FLAG_SPACE	8
# define FT_PRINTF_FLAG_MUL		16
# define FT_PRINTF_FLAG_ZERO	32

# define BIN_PREV				0
# define OCT_PREV				2
# define DEC_PREV				4
# define HEX_PREV				6
# define HEXB_PREV				8

# define BIN_BASE				1
# define OCT_BASE				3
# define DEC_BASE				5
# define HEX_BASE				7
# define HEXB_BASE				9

typedef struct	s_pfvar
{
	char		type;
	char		length[3];
	int			fd;
	int			flag;
	int			width;
	int			precision;
	size_t		bytes;
	va_list		ap;
}				t_pfvar;

typedef struct	s_pftypes
{
	char		type;
	size_t		(*f)(t_pfvar *);
}				t_pftypes;

static const char		*g_digits[] =
{
	"", "01",
	"0", "01234567",
	"", "0123456789",
	"0x", "0123456789abcdef",
	"0X", "0123456789ABCDEF"
};

int				ft_printf(const char *format, ...);
int				ft_fdprinf(const int fd, const char *format, ...);
int				ft_vfdprintf(const int fd, const char *format, va_list ap);
size_t			print_char(t_pfvar *var);
size_t			print_sdec(t_pfvar *var);
size_t			print_n(t_pfvar *var);
size_t			print_p(t_pfvar *var);
size_t			ft_print_token(t_pfvar *var);
size_t			ft_putl(const int fd, long nbr, const char *base);
size_t			ft_putul(const int fd, unsigned long nbr, const char *base);
size_t			ft_print_null(t_pfvar *var);
size_t			ft_putnb(const int fd, int n);
const char		*ft_token(const char *p, size_t *bytes, t_pfvar *var);
unsigned long	get_arg_long(t_pfvar *var);
size_t			ft_print_gen(t_pfvar *var, const char *pre, const char *base);
size_t			ft_printbin(t_pfvar *var);
size_t			ft_printoct(t_pfvar *var);
size_t			ft_printdec(t_pfvar *var);
size_t			ft_printhex(t_pfvar *var);
size_t			ft_printhexb(t_pfvar *var);
size_t			ft_print_char(t_pfvar *var);
size_t			ft_print_sdec(t_pfvar *var);
size_t			ft_print_p(t_pfvar *var);
size_t			ft_print_n(t_pfvar *var);
size_t			ft_fillspc(int pre, t_pfvar *var, int len);
size_t			ft_fillzero(t_pfvar *var, int len);
int				ft_abs(int n);
int				ft_pos(int n);
int				ft_max(int a, int b);
void			*ft_memcpy(void *dest, const void *src, size_t n);
void			*ft_memset(void *s, int c, size_t n);
long			ft_strtol(const char *p, const char **ret, int base);
int				ft_log(unsigned long n, int base);
int				ft_strpos(const char *str, char c);
int				ft_tolower(int c);
int				ft_toupper(int c);
int				ft_isalpha(int c);
int				ft_islower(int c);
int				ft_isupper(int c);
char			*ft_strchr(const char *s, int c);
size_t			ft_strlen(const char *str);
size_t			ft_putchar(char c);
size_t			ft_putnchar(const int fd, const char c, int n);
size_t			ft_putchar_fd(int fd, char c);
size_t			ft_putchar_utf8(const unsigned int cp);
size_t			ft_putstr(char const *s);
size_t			ft_print_str(t_pfvar *var);
size_t			ft_print_spestr(t_pfvar *var);
size_t			ft_putstr_fd(int fd, char const *s);

static const t_pftypes	g_types[] =
{
	{'%', ft_print_null},
	{'n', ft_print_n},
	{'p', ft_print_p},
	{'d', ft_print_sdec},
	{'i', ft_print_sdec},
	{'u', ft_printdec},
	{'o', ft_printoct},
	{'x', ft_printhex},
	{'X', ft_printhexb},
	{'b', ft_printbin},
	{'c', ft_print_char},
	{'C', ft_print_char},
	{'s', ft_print_str},
	{'S', ft_print_spestr},
	{0, 0},
};

#endif
