/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   handle_escape.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/16 18:16:44 by atheveno          #+#    #+#             */
/*   Updated: 2016/02/16 21:31:42 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "handle_funcs.h"
#include <libft.h>
#include "utils.h"

ssize_t	handle_escape(char **format, va_list *args, t_arg *arg)
{
	(void)format;
	(void)args;
	(void)arg;
	if (arg->got_width && !arg->right_pad)
		width_pad(1, arg->width, arg->pad_zeroes ? '0' : ' ', arg);
	ft_putchar('%');
	if (arg->got_width && arg->right_pad)
		width_pad(1, arg->width, ' ', arg);
	return (arg->got_width ? ft_max(arg->width, 1) : 1);
}
