/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putl.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/12 10:07:30 by atheveno          #+#    #+#             */
/*   Updated: 2016/02/12 10:08:00 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

static size_t	ft_putl_rec(const int fd, long n, const char *base,\
		int baselen)
{
	size_t	size;

	if (n == 0)
		return (0);
	size = ft_putl_rec(fd, n / baselen, base, baselen);
	if (n > 0)
		ft_putchar_fd(fd, base[n % baselen]);
	else
		ft_putchar_fd(fd, base[-1 * (n % baselen)]);
	return (size + 1);
}

size_t			ft_putl(const int fd, long nbr, const char *base)
{
	size_t	size;

	size = 0;
	if (nbr == 0)
		size += ft_putchar_fd(fd, base[0]);
	else
	{
		if (nbr < 0)
			size += ft_putchar_fd(fd, '-');
		size += ft_putl_rec(fd, nbr, base, ft_strlen(base));
	}
	return (size);
}
