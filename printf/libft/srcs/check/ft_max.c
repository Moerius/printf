/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_max.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/12 19:41:49 by atheveno          #+#    #+#             */
/*   Updated: 2016/02/12 19:42:08 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_max(int x, int y)
{
	return (x > y ? x : y);
}
