/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnchar.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/12 10:06:43 by atheveno          #+#    #+#             */
/*   Updated: 2016/02/12 10:07:13 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

size_t	ft_putnchar(const int fd, const char c, int n)
{
	int i;

	i = 0;
	while (i++ < n)
		write(fd, &c, 1);
	return (n);
}
