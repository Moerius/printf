/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strpos.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/12 10:09:35 by atheveno          #+#    #+#             */
/*   Updated: 2016/02/12 10:09:54 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

int	ft_strpos(const char *str, char c)
{
	const char	*origin;

	origin = str;
	while (*str)
	{
		if (*str == c)
			return (str - origin);
		str++;
	}
	return (str - origin);
}
