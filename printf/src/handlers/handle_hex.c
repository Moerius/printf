/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   handle_hex.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/16 18:17:00 by atheveno          #+#    #+#             */
/*   Updated: 2016/02/16 22:00:39 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdarg.h>
#include "ft_printf.h"
#include "handle_funcs.h"
#include "utils.h"

ssize_t	handle_hex(char **format, va_list *args, t_arg *arg)
{
	uintmax_t	nbr;

	nbr = get_unsigned_from_length(args, arg);
	if (**format == 'X')
		return (handle_uint(nbr, arg, "0123456789ABCDEF", "0X"));
	else
		return (handle_uint(nbr, arg, "0123456789abcdef", "0x"));
}
