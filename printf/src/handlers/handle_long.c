/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   handle_long.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/16 18:17:10 by atheveno          #+#    #+#             */
/*   Updated: 2016/02/16 18:17:11 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "handle_funcs.h"
#include "ft_printf.h"
#include <libft.h>

ssize_t	handle_long(char **format, va_list *args, t_arg *arg)
{
	arg->length = l;
	return (get_handler(ft_tolower(**format))(format, args, arg));
}
