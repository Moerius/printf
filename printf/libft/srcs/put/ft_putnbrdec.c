/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbrdec.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/10 18:09:56 by atheveno          #+#    #+#             */
/*   Updated: 2016/01/10 20:03:13 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/libft.h"

static const char	*g_decbase = "0123456789";

int		ft_putnbrdec(unsigned int nb, unsigned int len)
{
	char	nb_act;
	int		size;

	nb_act = nb % 10;
	size = 1;
	if (nb >= 10)
		size += ft_putnbrdec(nb / 10, len > 0 ? len - 1 : 0);
	else if (len > 0)
	{
		size += len - 1;
		while (--len > 0)
			ft_putchar('0');
	}
	ft_putchar(g_decbase[(int)nb_act]);
	return (size);
}
