/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/24 15:22:00 by atheveno          #+#    #+#             */
/*   Updated: 2016/01/09 14:21:41 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/libft.h"

void	*ft_memccpy(void *dest, const void *src, int c, size_t n)
{
	if (n && src)
	{
		while (n != 0)
		{
			if ((*(unsigned char *)dest++ = *(unsigned char *)src++)
				== (unsigned char)c)
				return (dest);
			--n;
		}
	}
	return (NULL);
}
