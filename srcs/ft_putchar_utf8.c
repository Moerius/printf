/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putchar_utf8.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/12 10:08:06 by atheveno          #+#    #+#             */
/*   Updated: 2016/02/12 10:08:07 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

size_t	ft_putchar_utf8(const unsigned int cp)
{
	if (cp < 128)
		ft_putchar(cp);
	else if (cp < 2048)
	{
		ft_putchar(192 | (cp >> 6));
		ft_putchar(128 | (cp & 63));
	}
	else if (cp < 65536)
	{
		ft_putchar(224 | (cp >> 12));
		ft_putchar(128 | ((cp >> 6) & 63));
		ft_putchar(128 | (cp & 63));
	}
	else if (cp < 1114112)
	{
		ft_putchar(240 | (cp >> 18));
		ft_putchar(128 | ((cp >> 12) & 63));
		ft_putchar(128 | ((cp >> 6) & 63));
		ft_putchar(128 | (cp & 63));
	}
	return (1);
}
