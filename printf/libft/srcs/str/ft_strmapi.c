/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 14:23:29 by atheveno          #+#    #+#             */
/*   Updated: 2016/01/09 14:30:41 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/libft.h"

char	*ft_strmapi(char const *s, char (*f)(unsigned int, char))
{
	char			*tmp;
	unsigned int	index;

	index = 0;
	tmp = ft_strnew(ft_strlen(s));
	if (tmp == NULL || s == NULL)
		return (NULL);
	while (s[index])
	{
		tmp[index] = f(index, s[index]);
		index++;
	}
	tmp[index] = '\0';
	return (tmp);
}
