/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fdprintf.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/12 09:58:53 by atheveno          #+#    #+#             */
/*   Updated: 2016/02/12 09:59:14 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

int	ft_fdprinf(const int fd, const char *format, ...)
{
	va_list ap;
	size_t	bytes;

	va_start(ap, format);
	bytes = ft_vfdprintf(fd, format, ap);
	va_end(ap);
	return (bytes);
}
