/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/24 13:06:56 by atheveno          #+#    #+#             */
/*   Updated: 2015/12/15 07:16:54 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

char	*ft_itoa(int c)
{
	char	*tmp;
	char	digit[11];
	int		sign;
	int		i;
	int		j;

	i = 0;
	j = 0;
	sign = (c < 0) ? -1 : 1;
	while (sign * c > 9 || sign * c < 0)
	{
		digit[i++] = '0' + sign * (c % 10);
		c = c / 10;
	}
	digit[i++] = '0' + sign * c;
	if (sign-- - 1)
		digit[i++] = '-';
	if (!(tmp = (char *)malloc(sizeof(char) * i + 1)))
		return (NULL);
	tmp[i] = '\0';
	while (i--)
		tmp[i] = digit[j++];
	return (tmp);
}
