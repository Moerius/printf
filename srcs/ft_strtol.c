/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtol.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/12 10:10:36 by atheveno          #+#    #+#             */
/*   Updated: 2016/02/12 10:13:12 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

static const char	*g_digit = "0123456789abcdefghijklmnopqrstuvwxyz";

static long			ft_strtol_rec(const char **str, int sign, int base)
{
	long	ret;
	int		i;

	ret = 0;
	while (**str && (i = ft_strpos(g_digit, ft_tolower(**str))) < base)
	{
		if (sign == -1)
			ret = ret * base - i;
		else
			ret = ret * base + i;
		(*str)++;
	}
	return (ret);
}

long				ft_strtol(const char *p, const char **ret, int base)
{
	int		sign;
	int		i;
	long	res;

	i = 0;
	sign = 1;
	while (p[i] == '-' || p[i] == '+')
	{
		if (p[i] == '-')
			sign = sign * -1;
		i++;
	}
	res = ft_strtol_rec(&p, sign, base);
	*ret = p;
	return (res);
}
