/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putul.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/12 10:08:43 by atheveno          #+#    #+#             */
/*   Updated: 2016/02/12 10:08:45 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

static size_t	ft_putul_rec(const int fd, unsigned long n, const char *base, \
		int baselen)
{
	size_t	size;

	if (n == 0)
		return (0);
	size = ft_putul_rec(fd, n / baselen, base, baselen);
	ft_putchar_fd(fd, base[n % baselen]);
	return (size + 1);
}

size_t			ft_putul(const int fd, unsigned long nbr, const char *base)
{
	size_t	size;

	size = 0;
	if (nbr == 0)
		size += ft_putchar_fd(fd, base[0]);
	else
		size += ft_putul_rec(fd, nbr, base, ft_strlen(base));
	return (size);
}
