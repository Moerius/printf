# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/11/28 17:18:20 by atheveno          #+#    #+#              #
#    Updated: 2016/02/12 10:29:15 by atheveno         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

.PHONY : all clean fclean re save norminette

NAME = 	libftprintf.a
SPATH = srcs
OPATH = bin
SRC = 
OBJ = 	$(addprefix $(OPATH)/, $(SRC:.c=.o))
EXT = 	./includes/$(NAME:.a=.h)
EXT +=	Makefile
NOW := $(shell date +"%c" | tr ' :' '_'))

SRC += ft_abs.c ft_atoi_base.c ft_fdprintf.c ft_fill.c ft_isalpha.c
SRC += ft_islower.c ft_isupper.c ft_log.c ft_max.c ft_memcpy.c ft_memset.c
SRC += ft_pos.c ft_print.c ft_print_gen.c ft_print_nbr.c ft_print_null.c
SRC += ft_print_token.c ft_printf.c ft_putchar.c ft_putchar_fd.c
SRC += ft_putchar_utf8.c ft_putl.c ft_putnb.c ft_putnchar.c 
SRC += ft_putstr.c ft_putstr_all.c ft_putstr_fd.c ft_putul.c ft_strchr.c
SRC += ft_strlen.c ft_strpos.c ft_strtol.c ft_tolower.c ft_toupper.c
SRC += ft_vfdprintf.c get_arg_long.c

vpath %.c srcs/

all: $(NAME)

test: re
	make -C tests

norminette:
	@echo "CHECKING NORMINETTE"
	@norminette **/*.[ch]

$(NAME): $(OBJ) $(EXT)
	@echo "-> Creating $(NAME)..."
	@ar rc $(NAME) $(OBJ)
	@echo "-> Optimising $(NAME)..."
	@ranlib $(NAME)
	@echo "-> Done !"

bin/%.o: %.c $(EXT) 
	@mkdir -p bin 
	@echo "-> Compiling $<..."
	@gcc -Werror -Wextra -Wall -I includes/ -c $< -o $@

clean: 
	@echo "-> Cleaning libft object files..."
	@/bin/rm -rf bin/

fclean: clean
	@echo "-> Cleaning $(NAME)..."
	@/bin/rm -rf libftprintf.a

re: fclean all

save:
		@git add --all
		@git commit -m 'saving $(NOW)'
		@echo "$(YELLOW)all files added and commited$(WHITE)"
