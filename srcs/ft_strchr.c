/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/12 10:08:51 by atheveno          #+#    #+#             */
/*   Updated: 2016/02/12 10:08:52 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

char	*ft_strchr(const char *s, int c)
{
	size_t index;
	size_t length;

	index = 0;
	length = ft_strlen(s);
	while (index <= length)
	{
		if (s[index] == (char)c)
			return ((char*)(s + index));
		index++;
	}
	return (NULL);
}
