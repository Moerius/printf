/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_nstrlen.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/12 19:45:40 by atheveno          #+#    #+#             */
/*   Updated: 2016/02/12 19:48:20 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/libft.h"

size_t	ft_nstrlen(const char *str, size_t maxlen)
{
	size_t i;

	i = 0;
	while (*str++ && i < maxlen)
		i++;
	return (i);
}
