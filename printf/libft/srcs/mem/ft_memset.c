/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/10/18 18:53:46 by atheveno          #+#    #+#             */
/*   Updated: 2016/01/09 14:24:31 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/libft.h"

void	*ft_memset(void *s, int c, size_t n)
{
	char	*tmp;

	if (n == 0)
		return (s);
	tmp = (char *)s;
	while (n--)
	{
		*tmp = (char)c;
		if (n)
			tmp++;
	}
	return (s);
}
