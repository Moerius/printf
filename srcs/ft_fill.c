/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fill.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/12 09:59:24 by atheveno          #+#    #+#             */
/*   Updated: 2016/02/12 09:59:35 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

size_t		ft_fillspc(int pre, t_pfvar *var, int len)
{
	size_t	bytes;

	bytes = 0;
	if ((!pre && (var->flag & FT_PRINTF_FLAG_MINUS))
			|| (pre && !(var->flag & FT_PRINTF_FLAG_MINUS)))
		bytes = ft_putnchar(var->fd, ' ', ft_pos(var->width - len));
	return (bytes);
}

size_t		ft_fillzero(t_pfvar *var, int len)
{
	size_t	bytes;

	bytes = ft_putnchar(var->fd, '0', ft_pos(var->precision - len));
	return (bytes);
}
