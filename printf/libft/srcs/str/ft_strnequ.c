/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnequ.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 14:52:02 by atheveno          #+#    #+#             */
/*   Updated: 2016/01/09 14:31:50 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/libft.h"

int	ft_strnequ(char const *s1, char const *s2, size_t n)
{
	size_t	index;

	index = 0;
	if (index < n)
	{
		if (!s1 || !s2)
			return (0);
		while (s1[index] && s2[index] && n--)
		{
			if (s1[index] == s2[index])
				index++;
			else
				return (0);
		}
		if (s1[index] != '\0' && s2[index] == '\0')
			return (0);
		else if (s1[index] == '\0' && s2[index] != '\0')
			return (0);
		else
			return (1);
	}
	return (1);
}
