/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putendl_fd.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/24 16:17:33 by atheveno          #+#    #+#             */
/*   Updated: 2016/01/29 01:04:51 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/libft.h"

size_t	ft_putendl_fd(char const *str, int fd)
{
	size_t i;

	i = ft_putstr_fd(str, fd);
	write(fd, "\n", 1);
	return (i);
}
