/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_token.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/12 10:04:45 by atheveno          #+#    #+#             */
/*   Updated: 2016/02/12 10:30:33 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

size_t		ft_print_token(t_pfvar *var)
{
	const t_pftypes *p;

	p = g_types;
	while (p->type)
	{
		if (var->type == p->type)
			return (p->f(var));
		p++;
	}
	return (ft_putchar_fd(var->fd, var->type));
}
