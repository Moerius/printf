/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcat.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/10/20 05:27:28 by atheveno          #+#    #+#             */
/*   Updated: 2016/01/09 14:28:29 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/libft.h"

char	*ft_strcat(char *dest, const char *src)
{
	size_t index1;
	size_t index2;

	index1 = 0;
	index2 = 0;
	while (dest[index1])
		index1++;
	while (src[index2])
	{
		dest[index1 + index2] = src[index2];
		index2++;
	}
	dest[index1 + index2] = 0;
	return (dest);
}
