/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/12 09:58:14 by atheveno          #+#    #+#             */
/*   Updated: 2016/02/12 10:28:57 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

static int	ft_atoi_base_rec(const char *str, int sign, \
		const char *base, int basen)
{
	int ret;
	int i;
	int j;

	j = 0;
	ret = 0;
	while (str[j] && (i = ft_strpos(base, str[j])) < basen)
	{
		if (sign == -1)
			ret = ret * basen - i;
		else
			ret = ret * basen + i;
		j++;
	}
	return (ret);
}

int			ft_atoi_base(const char *str, const char *base)
{
	int sign;
	int ret;
	int basen;
	int	i;

	i = 0;
	basen = ft_strlen(base);
	sign = 1;
	while (str[i] == '-' || str[i] == '+')
	{
		if (str[i] == '-')
			sign = sign * -1;
		i++;
	}
	ret = ft_atoi_base_rec(str, sign, base, basen);
	return (ret);
}
