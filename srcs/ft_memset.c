/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/12 10:01:27 by atheveno          #+#    #+#             */
/*   Updated: 2016/02/12 10:01:38 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

void	*ft_memset(void *s, int c, size_t n)
{
	char	*tmp;

	if (n == 0)
		return (s);
	tmp = (char *)s;
	while (n--)
	{
		*tmp = (char)c;
		if (n)
			tmp++;
	}
	return (s);
}
