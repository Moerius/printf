/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   handle_str.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/16 18:17:37 by atheveno          #+#    #+#             */
/*   Updated: 2016/02/16 21:32:23 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "handle_funcs.h"
#include "utils.h"
#include <libft.h>
#include <wchar.h>

ssize_t	handle_str(char **format, va_list *args, t_arg *arg)
{
	char	*str;
	size_t	strlen;

	if (arg->length == l)
		return (handle_wstr(format, args, arg));
	else
	{
		str = va_arg(*args, char*);
		if (str == NULL)
			str = "(null)";
		strlen = arg->got_precision ? ft_nstrlen(str, arg->precision) :
				ft_strlen(str);
		if (arg->got_width && !arg->right_pad)
			width_pad(strlen, arg->width, arg->pad_zeroes ? '0' : ' ', arg);
		ft_putnstr(str, strlen);
		if (arg->got_width && arg->right_pad)
			width_pad(strlen, arg->width, ' ', arg);
		return (arg->got_width ? ft_max(strlen, arg->width) : strlen);
	}
}
